// Import the the method.
const method = require('./method')

// Define the test data.
const products1 = [
  {id: 1, price: 10}, {id: 2, price: 11}, {id: 3, price: 1},
  {id: 4, price: 3}, {id: 5, price: 1}, {id: 6, price: 8},
  {id: 7, price: 3}, {id: 8, price: 0}, {id: 9, price: 4},
  {id: 10, price: 5}, {id: 11, price: 9}, {id: 12, price: 13}
]

const value1 = method(products1)

const products2 = [
  { id: 1, price: 10 },
  { id: 2, price: 11 },
  { id: 3, price: 1 }
]

const value2 = method(products2)

const products3 = [
  { id: 1, price: 10 },
  { id: 2, price: 11 },
  { id: 3, price: 1 },
  { id: 3, price: 1 },
  { id: 3, price: 1 },
  { id: 3, price: 1 }
]

const value3 = method(products3, { size: 4 })

const value4 = method(products3, { size: 4 })

// Define the function used to test our assertions.
const assert = (description, success) => {
  console.log(`${success ? 'Success' : '\nFailure'}: ${description}`)
}

// Define the funcion that confirms a variable is a defined object.
const isObject = variable => typeof variable === 'object' &&
  variable !== null &&
  variable !== '' &&
  !Array.isArray(variable) &&
  !(variable instanceof String)

// Define the function for testing the individual properties.
const hasTheRightProducts = (data, property, values, size) => data[property].length === size &&
  values.every((value, index) => data[property][index].price === value)

// Define the unit tests.
assert('The method should be defined.', method)

assert('The method should return an object.', isObject(value1))

assert(
  'The object should have highest and lowest properties.',
  ['highest', 'lowest'].every(property => value1.hasOwnProperty(property))
)

assert(
  'If the size is smaller than the product array, neither property should be populated.',
  value2.highest === null && value2.lowest === null
)

assert(
  'If there are not sufficient products to populate both properites, highest should be populated.',
  hasTheRightProducts(value3, 'highest', [11, 10, 1, 1], 4) &&
  hasTheRightProducts(value3, 'lowest', [1, 1], 2)
)

assert(
  'If there are sufficient products, the highest property should be the five highest priced.',
  hasTheRightProducts(value1, 'highest', [13, 11, 10, 9, 8], 5)
)

assert(
  'If there are sufficient products, the lowest property should be the five lowest priced.',
  hasTheRightProducts(value1, 'lowest', [0, 1, 1, 3, 3], 5)
)

assert(
  'If the method is repeated with all the same parameters, it should return null for both values.',
  value4.highest === null && value4.lowest == null
)
