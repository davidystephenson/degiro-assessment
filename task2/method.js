// TODO add property parameter
const descending = (a, b) => b.price - a.price
const ascending = (a, b) => a.price - b.price

const getTopSet = (array, sorter, size) => array.sort(sorter).slice(0, size)
const filterFrom = (a, b) => a.filter(element => !b.includes(element))

module.exports = (products, options = { size: 5 }) => {
  const output = {}

  const getTop = (array, sorter, mustFill) => {
    if (array.length > 0 && (!mustFill || array.length >= options.size)) {
      return getTopSet(array, sorter, options.size)
    } else {
      return null
    }
  }

  output.highest = getTop(products, descending, true)

  if (output.highest) {
    products = filterFrom(products, output.highest)
    output.lowest = getTop(products, ascending, false)
  } else {
    output.lowest = null
  }

  return output
}
