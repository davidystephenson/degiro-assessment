const descending = (a, b) => b.price - a.price
const ascending = (a, b) => a.price - b.price

const getTopSet = (array, sorter, size) => array.sort(sorter).slice(0, size)

module.exports = (products, options = { size: 5 }) => {
  const getTop = sorter => getTopSet(products, sorter, options.size)

  return { highest: getTop(descending), lowest: getTop(ascending) }
}
