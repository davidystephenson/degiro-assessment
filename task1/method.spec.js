// Import the the method.
const method = require('./method')

// Define the test data.
const products = [
  {id: 1, price: 10}, {id: 2, price: 11}, {id: 3, price: 1},
  {id: 4, price: 3}, {id: 5, price: 1}, {id: 6, price: 8},
  {id: 7, price: 3}, {id: 8, price: 0}, {id: 9, price: 4},
  {id: 10, price: 5}, {id: 11, price: 9}, {id: 12, price: 13}
]

const value = method(products)

// Define the function used to test our assertions.
const assert = (description, success) => {
  console.log(`${success ? 'Success' : '\nFailure'}: ${description}`)
}

// Define the funcion that confirms a variable is a defined object.
const isObject = variable => typeof variable === 'object' &&
  variable !== null &&
  variable !== '' &&
  !Array.isArray(variable) &&
  !(variable instanceof String)

// Define the function for testing the individual properties.
const hasTheRightFive = (data, property, values) => data[property].length === 5 &&
  values.every((value, index) => data[property][index].price === value)

// Define the unit tests.
assert('The method should be defined.', method)

assert('The method should return an object.', isObject(value))

assert(
  'The object should have highest and lowest properties.',
  ['highest', 'lowest'].every(property => value.hasOwnProperty(property))
)

assert(
  'The highest property should be a list of the five highest priced products',
  hasTheRightFive(value, 'highest', [13, 11, 10, 9, 8])
)

assert(
  'The lowest property should be a list of the five lowest priced products',
  hasTheRightFive(value, 'lowest', [0, 1, 1, 3, 3])
)
